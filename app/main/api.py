'''This module is an API that calls add and list functions'''
from flask import Flask, jsonify, request

app = Flask(__name__)

task_items = []

@app.route("/api/tasks", methods = ['GET', 'POST'])
def tasks():
    '''Checks which method is invoked and calls appropriate action mehtod'''
    if request.method == 'GET':
        return get_task_items()
    if request.method == 'POST':
        return add_task_item(request.get_json())
    return ""

@app.route('/api/tasks/clear', methods = ['DELETE'])
def clear_task_items():
    '''This function simply clears all the task items'''
    task_items.clear()
    return jsonify(task_items)

def get_task_items():
    '''Gets all the items in our task list'''
    return jsonify(task_items)

def add_task_item(task_item):
    '''Adds a new item to the task list'''
    new_id = len(task_items) + 1
    new_task_item = {
        "id": new_id,
        "title": task_item.get("title")
    }
    task_items.append(new_task_item)
    return jsonify(new_task_item)
