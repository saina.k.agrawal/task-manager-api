import requests

def test_insertop():
    r = requests.delete('http://127.0.0.1:5000/api/tasks/clear')

    r = requests.post('http://127.0.0.1:5000/api/tasks', json={"title": "Test Task 1"})
    r = requests.post('http://127.0.0.1:5000/api/tasks', json={"title": "Test Task 2"})
    r = requests.post('http://127.0.0.1:5000/api/tasks', json={"title": "Test Task 3"})
    r = requests.post('http://127.0.0.1:5000/api/tasks', json={"title": "Test Task 4"})

    r = requests.get('http://127.0.0.1:5000/api/tasks')

    assert(len(r.json()) == 4)
